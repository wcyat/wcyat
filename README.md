# wcyat
Hi, welcome to my page!

# About me
I created this page mainly for fun becuz it costs nothing, and maybe I can learn some javascript/typescript building the site. I am still a beginner to programming (been around for less than 3 months). Plz help me if you can, and well, want. 


# Github
find me on github: <br />
![icon](https://avatars.githubusercontent.com/u/87887997?s=120&v=4)
[wcyat](https://github.com/wcyat)

## Repositories
### C++
- [enable-hyperv-sandbox](https://github.com/wcyat/enable-hyperv-sandbox)
- [simple-password-generator](https://github.com/wcyat/simple-password-generator)
- [virus-simulation](https://github.com/wcyat/virus-simulation)

### Python
- [telegram-voting-bot](https://github.com/wcyat/telegram-voting-bot)
- [wendy-cheung](https://github.com/wcyat/wendy-cheung)

