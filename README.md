# wcyat

## Introduction

Hello, I am a ~~high-school~~ ~~software engineer~~ dental student, from Hong Kong.

## Timezone

UTC+8 (Asia/Hong_Kong), normally I am available from late afternoon to night.

## Skills

- hacking: newbee level
- blockchain: don't find me for that, also newbee, don't really like blockchain
- linux (~2 year of bare-metal usage), I use arch
- docker (used frequently)
- js/ts (quite very familiar, especially nodejs)
- web: react
- for server side I mainly use nodejs (ts)
- (still) newbee C++
- some knowledge in python>

## Work

Notable projects:

- [metahkg](https://gitlab.com/metahkg/metahkg): A forum that aims to be a fully featured alternative to lihkg (some 10 repositories that I have to maintain)
- [safecantonese.ai app](https://github.com/sdip15fa/safecantonese.ai.app): offline app for transcribing cantonese audio files
- [whatsbot](https://gitlab.com/wcyat/whatsbot): A heavily modified feature-rich fork of the original [Whatsbot](https://github.com/tuhinpal/WhatsBot/)
- [weather-predict](https://gitlab.com/wcyat/weather-predict): LSTM model to predict the temperature at Tai Mo Shan

Starting from the middle of 2022 I am suffering from fatigue every day, probably from both after-effects of covid and schoolwork, and as a result greatly reduced contributions (as you can see in my contributions map), also becoming too tired to build my own website as I have wanted to.

## Contact

- Don't ever call me, there's a high chance that I wouldn't pick up
- [telegram](https://t.me/dv4gr) is ok
- discord: wcyat#0555
- join [my discord server](https://discord.gg/cF5Pjgd9xK) if you want!
- [email](mailto:wcyat@wcyat.me)
- my [public gpg key](https://gitlab.com/wcyat.gpg)
